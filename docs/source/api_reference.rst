API Reference
-------------

This manual provides detailed information about classes and their
methods.

.. toctree::
    rst/thing
    rst/entry
    rst/s3i_connector
    rst/dt_factory
    rst/tools

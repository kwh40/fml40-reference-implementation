Role
=========================================
.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:


.. automodule:: ml.role
    :members:
    :special-members: __init__

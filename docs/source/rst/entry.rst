Entry
=========================================
.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:


.. automodule:: ml.entry
    :members:
    :special-members: __init__

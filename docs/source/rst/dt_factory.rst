DT Factory
=========================================
.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

.. automodule:: ml.dt_factory
    :members:
    :special-members: __init__
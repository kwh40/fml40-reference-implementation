.. Python Reference Implementation of fml40 documentation master file, created by
   sphinx-quickstart on Thu Nov 12 13:09:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation of the fml40 python reference implementation!
==========================================================================
The documentation is composed of some preliminaries for installation, a quick start and the reference implementation package.
We have implemented a permanent Digital Twin, which runs on our internal servers. In order to communicate with it, you can use the `hmi.py` in the folder `demo`

Table of Contents
-----------------
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   md/preliminaries.md
   md/quick_start.md
   api_reference
   ForestML4.0_reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

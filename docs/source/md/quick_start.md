# Example - Quickly develop a Digital Twin of Harvester (DT Harvester)
Here, we provide a brief guide on how to develop a DT Harvester, which consists of a location property, a property representing operation hours, and a service for accepting felling orders. 
All these features can be modeled using ForestML 4.0.

## Step 1: Serialize JSON file 
The model of Harvester (and also its Digital Twin) can be serialized as JSON, as shown below: 

```json
{
    "thingId": "",
    "policyId": "",
    "attributes": {
        "class": "ml40::Thing",
        "name": "Demo Harvester",
        "features": [
            {
                "class": "fml40::AcceptsFellingJobs"
            },
            {
                "class": "ml40::OperatingHours",
                "total": 0
            },
            {
                "class": "ml40::Location",
                "longitude": 0,
                "latitude": 0,
                "orientation": 0
            }
        ]
    }
}
```

## Step 2: Registration
Identity ensures global identification of DT Harvester and is necessary for connecting to the S³I.
[The S³I Manager](https://manager.s3i.vswf.dev) is a web-based service supporting configuration of identities which can be assigned to users and their Digital Twin.
Using the S³I manager, identities can be created, which is required to log in the S³I Manager. 
After logging in, Thing identity can be registered by clicking ***Create Thing***. 
The returned ***identifier*** and ***secret*** should be kept locally. 
Afterwards, a message queue and a cloud copy should be created to DT Harvester under ***Create***, such that DT Harvester is reachable via the S³I Broker and the current state of DT Harvester can be stored in the S³I Repository.

## Step 3: Runs demo_harvester.py
The from Step 2 obtained identifier and secret used to launch ***demo_harvester.py*** 
```
python demo_harvester.py -i OAUTH2_ID -s OAUTH2_SECRET
```

## Step 4: Check the state update 
To verify whether DT Harvester is currently running, one can use ***Explore*** in the S³I Manager to check the current state of DT Harvester.
There, the continuous changes in operation hours will be shown.  
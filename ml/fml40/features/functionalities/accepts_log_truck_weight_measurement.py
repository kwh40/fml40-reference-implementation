"""This module implements the class AcceptsLogTruckWeightMeasurement."""

from ml.ml40.features.functionalities.functionality import Functionality


class AcceptsLogTruckWeightMeasurement(Functionality):
    """This functionality signalizes that SoilMoistureMeasurements can be
    processed."""

    def __init__(self, namespace="fml40", name="", identifier="", parent=None):
        """Initializes the object.

        :param name:  Object name
        :param identifier: Identifier

        """
        super().__init__(
            namespace=namespace, name=name, identifier=identifier, parent=parent
        )

    def accept(self, input):
        """Accepts the given measurement."""
        pass

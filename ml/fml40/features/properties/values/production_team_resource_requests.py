from ml.ml40.features.properties.values.value import Value
from typing_extensions import TypedDict
from typing import List

class EquipmentRequest(TypedDict):
    type: str
    request: str

class ProductionTeamResourceRequests(Value):

    __machineType: str
    __equipmentRequests = List[EquipmentRequest]
    def __init__(self, namespace="fml40", name="", identifier="", parent=None):
        super().__init__(
            namespace=namespace, name=name, identifier=identifier, parent=parent
        )
        self.__machineType = ""
        self.__equipmentRequests = []
        self.__json_out = dict()

    @property
    def machineType(self):
        return self.__machineType

    @machineType.setter
    def machineType(self, newType):
        self.__machineType = newType

    @property
    def equipmentRequests(self):
        return self.__equipmentRequests

    @equipmentRequests.setter
    def equipmentRequests(self, newRequest):
        self.__equipmentRequests = newRequest

    def to_json(self):
        self.__json_out = super().to_json()
        if self.__machineType is not None:
            self.__json_out["machineType"] = self.__machineType
        if self.__equipmentRequests is not None:
            self.__json_out["equipmentRequests"] = self.__equipmentRequests
        return self.__json_out


from enum import Enum


class CustomerType(Enum):
    ForestOwner = 0
    Enterprise = 1
    ForwardingAgency = 2
    Sawmill = 3

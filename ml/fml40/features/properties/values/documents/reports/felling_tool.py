from enum import Enum


class FellingTool(Enum):
    WINCH = 0
    WEDGE = 1

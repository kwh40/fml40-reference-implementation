from ml.ml40.roles.dts.parts.part import Part


class Cantilever(Part):
    def __init__(self, namespace="mml40", name="", identifier=""):
        super().__init__(namespace=namespace, name=name, identifier=identifier)

from enum import Enum


class JobStatus(Enum):
    Pending = 0
    InProgress = 1
    Complete = 2

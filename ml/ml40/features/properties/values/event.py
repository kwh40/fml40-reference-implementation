from ml.ml40.features.properties.property import Property
from genson import SchemaBuilder

class Event(Property):
    """Generic implementation of an Event."""

    def __init__(self, namespace="ml40", name="", identifier="", parent=None):
        super().__init__(
            namespace=namespace, name=name, identifier=identifier, parent=parent
        )
        self.__topic = None
        self.__description = None
        self.__frequency = None
        self.__schema = None
        self.__example_content = None

    @property
    def topic(self):
        return self.__topic

    @topic.setter
    def topic(self, value):
        self.__topic = value

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, value):
        self.__description = value

    @property
    def frequency(self):
        return self.__frequency

    @frequency.setter
    def frequency(self, value):
        self.__frequency = value

    @property
    def schema(self):
        return self.__schema

    @schema.setter
    def schema(self, value):
        self.__schema = value

    @property
    def exampleContent(self):
        return self.__example_content

    @exampleContent.setter
    def exampleContent(self, value: dict):
        self.__example_content = value
        if isinstance(self.__example_content, dict):
            builder = SchemaBuilder()
            builder.add_schema({
                "type": "object",
                "properties": {}
            })
            builder.add_object(self.__example_content)
            self.__schema = builder.to_schema()
        else:
            raise ValueError

    def to_json(self):
        self.__json_out = super().to_json()
        if self.__topic is not None:
            self.__json_out["topic"] = self.__topic
        if self.__description is not None:
            self.__json_out["description"] = self.__description
        if self.__frequency is not None:
            self.__json_out["frequency"] = self.__frequency
        if self.__schema is not None:
            self.__json_out["schema"] = self.__schema
        return self.__json_out


from enum import Enum


class FuelType(Enum):
    Diesel = 0
    Petrol = 1
    EngineOil = 2
    GearOil = 3

from ml.ml40.features.properties.property import Property


class EventList(Property):
    """Generic implementation of an Event."""

    def __init__(self, namespace="ml40", name="", identifier="", parent=None):
        super().__init__(
            namespace=namespace, name=name, identifier=identifier, parent=parent
        )
